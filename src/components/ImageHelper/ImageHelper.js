import React from "react";
import { API } from "../../backend";
import img from '../../logo.png'

const ImageHelper = ({className="image",product}) => {
    
    
  const imageurl = product._id
    ? `${API}/product/photo/${product._id}`
    : img;
  return (
      <img
        src={imageurl}
        alt="photo"
        style={{ maxHeight: "100%", maxWidth: "100%" }}
        className={className}
      />
  );
};

export default ImageHelper;