import "./auth.css";
import { connect } from "react-redux";
import { logoutUser } from "../../redux/actions/userAction";
import React, { Component } from "react";
import PropTypes from "prop-types";

const AuthNavItems = (props, credentials) => {
  const handleLogout = () => {
    props.logoutUser();
  };

  const NavItem = (props) => {
    return (
      <div className="dropdown">
        <div className="dropbtn">
          {props.label} <i className="fa fa-caret-down"></i>
        </div>
        <div className="dropdown-content">
          <div className="hello-text">
            <div>Hello, {props.label}</div>
          </div>
          <a href="/home">Home</a>
          <a href="/order">My Orders</a>

          {props.credentials.role === 0 ? (
            <a href="/becomeaseller">Become a Seller</a>
          ) : (
            ""
          )}
          {props.credentials.role === 1 ? (
            <a href="/viewproducts">View your Prodcut</a>
          ) : (
            ""
          )}
          <a href="/" onClick={handleLogout}>
            Logout
          </a>
        </div>
      </div>
    );
  };
  return (
    <div className="nav-items">
      {console.log("Cred", props.credentials)}
      {props.items.map((item) => (
        <NavItem
          label={item.label}
          key={item.id}
          id={item.id}
          to={item.to}
          credentials={props.credentials}
          dropdown={item.dropdown}
        />
      ))}
    </div>
  );
};

const mapStateToProps = (state) => ({
  credentials: state.user.credentials,
});

const mapActionToProps = {
  logoutUser,
};
AuthNavItems.propTypes = {
  user: PropTypes.object,
};

export default connect(mapStateToProps, mapActionToProps)(AuthNavItems);
