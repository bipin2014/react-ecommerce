import React, { useState } from "react";
import axios from "axios";
import { NotificationManager } from "react-notifications";
import { API } from "../../backend";

const Signup = (props) => {
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");
  const [cpassword, setCPassword] = useState("");
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [errors, setErrors] = useState([]);
  const [referal, setReferal] = useState("");
  const [loading, setLoading] = useState(false);

  const showLoading = () => <p>Loading</p>;

  const handleSubmit = (event) => {
    event.preventDefault();
    if (password !== cpassword) {
      alert(`Confirm Password Doesnot Match`);
      return;
    }
    setLoading(true);
    let newUserDetail = {
      email: email,
      password: password,
      confirmPassword: cpassword,
      name: firstName,
      lastname: lastName,
    };
    console.log(newUserDetail);

    axios
      .post(`${API}/signup`, newUserDetail)
      .then((data) => {
        if (data.data.error) {
          console.log(data.data.error);
          setErrors(data.data);
        } else {
          props.history.push("/login");
          console.log(data.data);
          setLoading(false);
          NotificationManager.success("Logged In", "Successful!", 2000);
        }
      })
      .catch((err) => {
        setLoading(false);
        console.error(err.code);
      });
  };

  return (
    <div className="content">
      <div className="auth-container">
        <h1>Signup</h1>
        <form onSubmit={handleSubmit}>
          First Name:
          <input
            type="text"
            name="first"
            placeholder="Enter FirstName"
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
            required
          />{" "}
          <br />
          Last Name:
          <input
            type="text"
            name="last"
            placeholder="Enter LastName"
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
            required
          />{" "}
          <br />
          Email:
          <input
            type="email"
            name="email"
            placeholder="Enter Email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            required
          />{" "}
          <br />
          Password:
          <input
            type="password"
            name="password"
            placeholder="Enter password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            required
          />
          <br />
          Confirm Password:
          <input
            type="password"
            name="cpassword"
            placeholder="Enter Confirm password"
            value={cpassword}
            onChange={(e) => setCPassword(e.target.value)}
            required
          />
          <br />
          {errors.error && <div className="error">{errors.error}</div>}
          {loading ? showLoading() : <button type="submit">Signup</button>}
          <div>
            <span>Or Already Have Account?</span>
            <a href="/login">Login</a>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Signup;
