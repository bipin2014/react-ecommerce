import React, { useState } from "react";
import { loginUser } from "../../redux/actions/userAction";
import { connect } from "react-redux";
import LoadingSpinner from "../../components/LoadingSpinner/LoadingSpinner";

const Login = (props) => {
  const [values, setValues] = useState({
    email: "bipin@gmail.com",
    password: "12345678",
    error: "",
    loading: false,
  });
  //Destructure
  const { email, password, error, loading } = values;

  const handleChange = () => (event) => {
    console.log(event.target.value);

    setValues({
      ...values,
      error: false,
      [event.target.name]: event.target.value,
    });
  };

  const handleSubmit = (event) => {
    event.preventDefault();
    setValues({
      ...values,
      loading: true,
    });
    let userDetail = {
      email: email,
      password: password,
    };

    console.log(userDetail);
    props.loginUser(userDetail);
    setValues({
      ...values,
      loading: false,
    });
  };

  return (
    <div className="content">
      <div className="auth-container">
        <h1>Login</h1>
        <form onSubmit={handleSubmit}>
          <label>Email:</label>
          <input
            type="email"
            name="email"
            placeholder="Enter Email"
            value={email}
            onChange={handleChange("email")}
            required
          />{" "}
          <br />
          <label>Password:</label>
          <input
            type="password"
            name="password"
            placeholder="Enter password"
            value={password}
            onChange={handleChange("password")}
            required
          />
          <br />
          {error && <div className="error">{error}</div>}
          {loading ? (
            <div>
              <button disabled>Login</button>
              <LoadingSpinner />
            </div>
          ) : (
            <button type="submit">Login</button>
          )}
          <div>
            <span>Or Don't Have Account?</span>
            <a href="/signup">SignUp</a>
          </div>
        </form>
      </div>
    </div>
  );
};

const mapStateToProps = (state) => ({
  user: state.user,
  UI: state.UI,
});

const mapActionsToProps = {
  loginUser,
};

export default connect(mapStateToProps, mapActionsToProps)(Login);
