import React, { Component } from 'react';
import axios from 'axios';
import img from '../../logo.png'
import { API, API_IMG } from '../../backend';
import ImageHelper from '../../components/ImageHelper/ImageHelper';
import { isAuthenticated } from '../../redux/actions/userAction';
import LoadingSpinner from '../../components/LoadingSpinner/LoadingSpinner';


class ViewOwnProduct extends Component {
    
    state = {
        products: []
    }
    

    componentDidMount() {
        this.getData();
    }

    getData = () => {
        axios.get(`${API}/products`).then(res => {
            console.log(res.data);
            this.setState({
                products: res.data
            })

        }).catch(err => {
            console.log(err.code);

        })
    }
    render() {
        const {user}=isAuthenticated();
        const handleDelete = (id) => {
            axios.delete(`${API}/product/${id}/${user._id}`).then(res => {
                console.log(res.data);
                this.getData();
            }).catch(err => {
                console.log(err.code);
            })
        }
        const handleEdit = (pid) => {
            this.props.history.push('/editProduct/' + pid);
        }

        const handleAdd = () => {
            this.props.history.push('/addProducts');
        }
        return (
            <div className="content">
                <div className="add-container">
                    <div>Product You have Added</div>
                    <button onClick={handleAdd}>Add New</button>
                </div>

                <div className="own-container">
                    {this.state.products.length>0?this.state.products.map(product => (
                        <div className="product">
                            <ImageHelper product={product}/>
                            <h3 className="products_name">{product.name}</h3>
                            <div className="products_price">Price : ${product.price}</div>
                            <div className="button-container">
                                <button className="edit" onClick={handleEdit.bind(this, product._id)}>Edit product</button>
                                <button className="delete" onClick={handleDelete.bind(this, product._id)}>Delete product</button>
                            </div>
                        </div>
                    )):<LoadingSpinner/>}
                </div>
            </div>
        )
    }
}

export default ViewOwnProduct;
