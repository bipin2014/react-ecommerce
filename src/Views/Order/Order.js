import React, { useState, useEffect } from "react";
import axios from "axios";

import ShowOrders from "../../containers/ShowOrders/ShowOrders";
import { API } from "../../backend";
import { isAuthenticated } from "../../redux/actions/userAction";
import LoadingSpinner from "../../components/LoadingSpinner/LoadingSpinner";

const Order = () => {
  const [orders, setOrders] = useState([]);

  useEffect(() => {
    const { token, user } = isAuthenticated();
    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
    axios.get(`${API}/order/all/${user._id}`).then((res) => {
      setOrders(res.data);
      console.log("Order", res.data);
    });
  }, []);

  return (
    <div className="content">
      <div className="section-heading">Current Order</div>

      {orders.length > 0 ? (
        orders.map((data) => <ShowOrders data={data} />)
      ) : (
        <div className="section-heading">
          <LoadingSpinner />
        </div>
      )}
    </div>
  );
};

export default Order;
