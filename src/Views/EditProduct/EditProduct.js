import React, { useState, useEffect } from "react";
import axios from "axios";
import MultipleValueTextInput from "react-multivalue-text-input";
import { API } from "../../backend";
import { isAuthenticated } from "../../redux/actions/userAction";

const EditProduct = (props) => {
  const [productname, setProductname] = useState("");
  const [description, setDescription] = useState("");
  const [price, setPrice] = useState(0);
  const [brand, setBrand] = useState("");
  const [warranty, setWarranty] = useState(0);
  const [stock, setStock] = useState(0);
  const [errors, setErrors] = useState({});
  const [image, setImage] = useState(null);

  const [category, setCategory] = useState([]);

  useEffect(() => {
    axios.get(`${API}/product/${props.match.params.id}`).then((res) => {
      console.log("Hellooo", res.data);

      setProductname(res.data.name);
      setDescription(res.data.description);
      setPrice(res.data.price);
      setBrand(res.data.brand);
      setWarranty(res.data.warranty);
      setCategory(res.data.category);
      setStock(res.data.stock);
    });
  }, []);

  const handleSubmit = (event) => {
    event.preventDefault();


    let bodyFormData = new FormData();

    bodyFormData.append("photo", image);
    bodyFormData.append("name", productname);
    bodyFormData.append("description", description);
    bodyFormData.append("price", parseInt(price));
    bodyFormData.append("brand", brand);
    bodyFormData.append("category", category);
    bodyFormData.append("warranty", warranty);
    bodyFormData.append("stock", parseInt(stock));

    const {token, user } = isAuthenticated();

    axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;

    axios
      .put(`${API}/product/${props.match.params.id}/${user._id}`, bodyFormData)
      .then((data) => {
        console.log(data.data);
        props.history.push("/viewproducts");
      })
      .catch((err) => {
        console.error(err.code);
      });
  };
  return (
    <div className="content">
      <div className="add-product">
        <h1>Edit Product</h1>
        <form onSubmit={handleSubmit}>
          Name:{" "}
          <input
            type="text"
            value={productname}
            name="name"
            placeholder="Enter Name of product"
            onChange={(e) => setProductname(e.target.value)}
          />
          <br />
          Description:{" "}
          <textarea
            placeholder="Enter Detail description"
            onChange={(e) => setDescription(e.target.value)}
            value={description}
          ></textarea>
          <br />
          price:{" "}
          <input
            type="text"
            name="price"
            value={price}
            placeholder="Enter price of product"
            onChange={(e) => setPrice(e.target.value)}
          />
          <br />
          Brand:{" "}
          <input
            type="text"
            name="brand"
            value={brand}
            placeholder="Enter Brand of product"
            onChange={(e) => setBrand(e.target.value)}
          />
          <br />
          Warranty:{" "}
          <input
            type="text"
            name="warranty"
            value={warranty}
            placeholder="Enter Warranty of product"
            onChange={(e) => setWarranty(e.target.value)}
          />
          <br />
          <MultipleValueTextInput
            onItemAdded={(item, allCat) => setCategory(allCat)}
            onItemDeleted={(item, allCat) => setCategory(allCat)}
            label="Category"
            name="category"
            className="minput"
            values={category}
            placeholder="Enter whatever items you want; separate them with COMMA or ENTER."
          />
          Stock:{" "}
          <input
            type="text"
            value={stock}
            name="deliveryCharge"
            placeholder="Enter DeliveryCharge of product"
            onChange={(e) => setStock(e.target.value)}
          />
          <br />
          Image:{" "}
          <input
            type="file"
            name="file"
            onChange={(e) => setImage(e.target.files[0])}
          />
          <br />
          <button type="submit">Edit Product</button>
        </form>
      </div>
    </div>
  );
};

export default EditProduct;
