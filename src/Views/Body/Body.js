import axios from "axios";
import ImageSlider from "../../components/ImageSlider/ImageSlider";
import { connect } from "react-redux";
import React, { useEffect,useState } from "react";
import { addToCart } from "../../redux/actions/cartAction";
import { Link } from "react-router-dom";
import { NotificationManager } from "react-notifications";
import { API, API_IMG } from "../../backend";
import ImageHelper from "../../components/ImageHelper/ImageHelper";
import { isAuthenticated } from "../../redux/actions/userAction";
import LoadingSpinner from "../../components/LoadingSpinner/LoadingSpinner";


const Body = (props) => {
  const [products, setproducts] = useState([
  ]);
  const [productsforyou, setProductsforyou] = useState([]);
  useEffect(() => {
    axios
      .get(`${API}/products`)
      .then((data) => {
        console.log(data.data);
        setproducts(data.data)
      })
      .catch((err) => console.error(err));

    axios
      .get(`${API}/products`)
      .then((data) => {
        console.log(data.data);
        setProductsforyou(data.data)
      })
      .catch((err) => console.error(err));
  }, []);

  const {token,user} = isAuthenticated();
  

  const handleCartAdd = (p) => {
    if (props.authenticated) {
      let cartData = {
        product: p._id,
        user:user._id,
        quantity: 1,
        price: p.price,
        total: p.price,
      };
      console.log(cartData);
      props.addToCart(cartData);
      NotificationManager.success(
        "Product added to Cart Sucessfully!",
        "Successful!",
        1000
      );
    } else {
      this.props.history.push("/login");
    }
  };
  return (
    <div className="content">
      <ImageSlider />
      <div className="section-heading">Our Trending Products</div>
      <div className="products">

        {products.length>0? products.map((product) => (
          <div key={product._id} className="product">
            <Link to={`/product/${product._id}`} className="link">
            <ImageHelper product={product}/>
              <h3 className="products_name">{product.name}</h3>
              <div className="products_price">Price : ${product.price}</div>
            </Link>
            <div className="add-to-cart-container">
              <i
                className="fa fa-cart-plus"
                onClick={handleCartAdd.bind(this, product)}
                aria-hidden="true"
              ></i>
            </div>
          </div>
        )):
      <LoadingSpinner/>}
      </div>

      
    </div>
  );
};

const mapActionsToProps = {
  addToCart,
};
const mapStateToProps = (state) => ({
  authenticated: state.user.authenticated,
});

export default connect(mapStateToProps, mapActionsToProps)(Body);
