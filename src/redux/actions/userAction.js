import {
  SET_USER,
  SET_ERRORS,
  CLEAR_ERRORS,
  LOADING_UI,
  LOADING_USER,
  SET_AUTHENTICATED,
  SET_UNAUTHENTICATED,
  LOADING_DATA,
} from "../types";
import axios from "axios";
import { NotificationManager } from "react-notifications";
import { API } from "../../backend";

export const loginUser = (userData, history) => (dispatch) => {
  dispatch({ type: LOADING_UI });
  axios
    .post(`${API}/signin`, userData)
    .then((res) => {
      if (res.data.error) {
        dispatch({
          type: SET_ERRORS,
          payload: res.data,
        });
        // NotificationManager.error(res.data.error, "Successful!", 2000);
      } else {
        // setAuthorizationHeader(res.data.token);
        console.log(res.data);
        authenticate(res.data, () => {
          
        });

        // dispatch(getUserData());
        dispatch({ type: CLEAR_ERRORS });
        NotificationManager.success("Logged In", "Successful!", 1000);
        window.location.href = "/";
      }
    })
    .catch((err) => {
      dispatch({
        type: SET_ERRORS,
        payload: err,
      });
      // NotificationManager.error(err, "Successful!", 2000);
    });
};

// const setAuthorizationHeader = (token) => {
//   localStorage.setItem("AUTH-TOKEN", token);
//   axios.defaults.headers.common["Authorization"] = `Bearer ${token}`;
// };

const authenticate = (data, next) => {
  if (typeof window !== "undefined") {
    localStorage.setItem("jwt", JSON.stringify(data));
    next();
  }
};
export const isAuthenticated = () => {
  if (typeof window == "undefined") {
    return false;
  }
  if (localStorage.getItem("jwt")) {
    return JSON.parse(localStorage.getItem("jwt"));
  } else {
    return false;
  }
};

export const getUserData = () => (dispatch) => {
  // dispatch({ type: LOADING_USER });
  const user = isAuthenticated();
  console.log("SEE", user.user);

  dispatch({
    type: SET_USER,
    payload: user.user,
  });

  //   axios
  //     .get(`${API}/user/:userId`, {
  //       headers: {
  //         'Authorization': `Bearer ${token}`,
  //       },
  //     })
  //     .then((res) => {
  //       console.log("DATATAATA", res.data);

  //     })
  //     .catch((err) => console.log(err));
};

// add reward Points
export const updateRewardPoint = (pointData) => (dispatch) => {
  dispatch({ type: LOADING_DATA });

  axios
    .patch(`${API}/user/add/rewardPoints`, pointData)
    .then((res) => {
      // dispatch({
      //     type: SET_POINTS,
      //     payload: res.data
      // });

      dispatch(getUserData());

      console.log(res.data);
    })
    .catch((err) => console.error(err));
};

// add reward Points
export const rechargeCard = (hashData) => (dispatch) => {
  dispatch({ type: LOADING_DATA });

  axios
    .post(`${API}/payment/addCurrency`, hashData)
    .then((res) => {
      console.log("console.error(err)ayoi");
      if (res.error) {
      } else {
        dispatch(getUserData());
        //
      }

      console.log(res.data);
    })
    .catch((err) => {
      console.error(err);
      console.log("kjashkjfakjbkajdbf");
    });
};

export const logoutUser = () => (dispatch) => {
  console.log("logout");
  localStorage.removeItem("jwt");
  delete axios.defaults.headers.common["Authorization"];
  dispatch({ type: SET_UNAUTHENTICATED });
};
