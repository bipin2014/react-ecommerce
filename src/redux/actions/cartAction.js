import {
    SET_CART,
    LOADING_DATA,
    SET_TOTAL,
} from '../types';
import axios from 'axios';
import { API } from '../../backend';
import { isAuthenticated } from './userAction';
const token=isAuthenticated();

export const getUserCart = () => (dispatch) => {
    // dispatch({ type: LOADING_DATA });
    // axios.defaults.headers.common["Authorization"] = `Bearer ${token.token}`;
    
    

    axios.get(`${API}/cart/getUserAllProductsInCart/${token.user._id}`).then(res => {
        dispatch({
            type: SET_CART,
            payload: res.data
        });
        console.log("Hello from cart");

        let total = 0;
        console.log("Cart",res.data);
        res.data.forEach(item => (
            total += item.price * item.quantity));
        console.log("Total",total);

        dispatch({
            type: SET_TOTAL,
            total: total
        });
    }).catch(err => console.error(err));
};

export const addToCart = (cartData) => (dispatch) => {
    dispatch({ type: LOADING_DATA });

    axios.post(`${API}/cart/add`, cartData).then(data => {
        console.log("Hello cart");
        dispatch(getUserCart())
    }).catch(err => {
        console.log(err.code);
    });
};

export const removeFromCart = (cartId) => (dispatch) => {
    dispatch({ type: LOADING_DATA });
    console.log("Remove");
    

    axios.delete(`${API}/cart/remove/${cartId}`).then(data => {
        dispatch(getUserCart())
    }).catch(err => {
        console.log(err);
    });
};

export const removeAllCart = () => (dispatch) => {
    dispatch({ type: LOADING_DATA });

    axios.delete(`${API}/cart/removeAll/${token.user._id}`).then(data => {
        dispatch(getUserCart())
    }).catch(err => {
        console.log(err.code);
    });
};

