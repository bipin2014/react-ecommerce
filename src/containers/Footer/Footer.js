import React from "react";
import img from '../../logodark.png'
import {Link} from 'react-router-dom'

function Footer() {
  return (
    <div className="footer-container">
      <div className="logo">
        <Link to="/">
          <img src={img} alt="Logo"></img>
        </Link>
      </div>
      <div className="about-us">
        <h2>About Us</h2>
        <p>
          Deal Mart is a Online Shopping System where user can buy and sell
          their product in a convienent manner. Handle your bussiness smartly
          and join us today. You will not regret your decision because we
          provide best service in the town.
        </p>
      </div>
    </div>
  );
}
export default Footer;
