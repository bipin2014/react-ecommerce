import React, { Component } from "react";
import axios from "axios";
import img from "../../logo.png";
import { Link } from "react-router-dom";
import { connect } from "react-redux";
import { addToCart } from "../../redux/actions/cartAction";
import { NotificationManager } from "react-notifications";
import { API, API_IMG } from "../../backend";
import { useState } from "react";
import { useEffect } from "react";
import ImageHelper from "../../components/ImageHelper/ImageHelper";
import { isAuthenticated } from "../../redux/actions/userAction";
import LoadingSpinner from "../../components/LoadingSpinner/LoadingSpinner";

const ProductDetails = (props) => {
  const [product, setProduct] = useState();
  const [products, setProducts] = useState([]);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    setLoading(true)
    let pid = props.match.params.productId;
    axios
      .get(`${API}/product/${pid}`)
      .then((res) => {
        console.log("Product details", res.data);
        setProduct(res.data);
        setLoading(false)
      })
      .catch((err) => console.error(err));

    let limit = 4;

    axios
      .get(`${API}/products`)
      .then((data) => {
        console.log(data.data);
        setProducts(data.data);
      })
      .catch((err) => console.error(err));
  }, [props.match.params.productId]);

  const handleCartAdd = (p) => {
    const { token, user } = isAuthenticated();
    if (props.authenticated) {
      let cartData = {
        user: user._id,
        product: p._id,
        quantity: 1,
        price: p.price,
        total: p.price,
      };
      console.log(cartData);
      props.addToCart(cartData);
      NotificationManager.success(
        "Product added to Cart Sucessfully!",
        "Successful!",
        2000
      );
    } else {
      props.history.push("/login");
    }
  };
  return (
    <div className="content">
      {loading ? (
        <div className="products"><LoadingSpinner/></div>
        
      ) : (
        <div className="product-container">
          <div className="image-area">
            <ImageHelper product={product} />
          </div>
          <div className="product-details">
            <h1 className="product-name">{product.name}</h1>

            <div className="product-category">
              Category:
              {product.category ? (
                <span>{product.category.map((d) => d)}</span>
              ) : (
                ""
              )}
            </div>
            <h2 className="product-price">${product.price}</h2>
            <div className="line"></div>
            <div className="product-description">{product.description}</div>
            <div className="line"></div>
            <button onClick={handleCartAdd.bind(this, product)}>
              ADD TO CART
            </button>
          </div>
        </div>
      )}

      <div className="section-heading">Other Related Products</div>
      <div className="products">
        {products.map((product) => (
          <div key={product._id} className="product">
            <Link to={`/product/${product._id}`} className="link">
              <ImageHelper product={product} />
              <h3 className="products_name">{product.name}</h3>
              <div className="products_price">Price : ${product.price}</div>
            </Link>
            <div className="add-to-cart-container">
              <i
                className="fa fa-cart-plus"
                onClick={handleCartAdd.bind(this, product)}
                aria-hidden="true"
              ></i>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

const mapActionsToProps = {
  addToCart,
};

const mapStateToProps = (state) => ({
  authenticated: state.user.authenticated,
});

export default connect(mapStateToProps, mapActionsToProps)(ProductDetails);
