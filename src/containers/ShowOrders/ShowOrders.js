import React from "react";
import img from "../../logo.png";
import dayjs from "dayjs";
import relativeTime from "dayjs/plugin/relativeTime";
import ImageHelper from "../../components/ImageHelper/ImageHelper";

const ShowOrders = (props) => {
  dayjs.extend(relativeTime);
  return (
    <div className="order-container">
      <div className="order-details">
        <div>Order Id:{props.data._id}</div>
        <div>Order Status:{props.data.status}</div>
        <div>Address:{props.data.address} </div>
        <div>Amount: {props.data.amount}</div>
        <div>Phone: {props.data.phone}</div>
        <div className="date">{dayjs(props.data.createdAt).fromNow()}</div>
        {console.log("Products",props.data)}
      </div>
      <div className="products">
        {props.data.products.length > 0 &&
          props.data.products.map((d) => (
            <div className="product">
              
              <ImageHelper product={d.product} />
              <div className="products_name">{d.product.name}</div>
            </div>
          ))}
      </div>
    </div>
  );
};

export default ShowOrders;
