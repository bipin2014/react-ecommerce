import React, { useState, useEffect } from "react";
import Navbar from "../../containers/NavBar/NavBar";
import Body from "../../Views/Body/Body";
import Footer from "../../containers/Footer/Footer";
import jwtDecode from "jwt-decode";
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import "../../assets/css/layout.scss";

import Login from "../../Views/Login/Login";
import Signup from "../../Views/Signup/Signup";
import Cart from "../../Views/Cart/Cart";
import Checkout from "../../Views/Checkout/Checkout";
import AuthRoute from "../../components/AuthRoute/AuthRoute";
import CheckAuthRoute from "../../components/CheckAuthRoute/CheckAuthRoute";
import AddProduct from "../../components/AddProduct/AddProduct";
import Order from "../../Views/Order/Order";
import axios from "axios";
import store from "../../redux/store";
import { SET_AUTHENTICATED } from "../../redux/types";
import {
  logoutUser,
  getUserData,
  isAuthenticated,
} from "../../redux/actions/userAction";
import { getUserCart } from "../../redux/actions/cartAction";
import ProductDetails from "../../containers/ProductDetails/ProductDetails";
import ProductSearch from "../../containers/ProductSearch/ProductSearch";
import ThankYou from "../../Views/ThankYou/ThankYou";
import ViewOwnProduct from "../../Views/ViewOwnProduct/ViewOwnProduct";
import VerifySeller from "../../Views/VerifySeller/VerifySeller";
import EditProduct from "../../Views/EditProduct/EditProduct";
import MyReferal from "../../Views/MyReferal/MyReferal";

import "react-notifications/lib/notifications.css";
import PropTypes from "prop-types";
import { connect } from "react-redux";
import AddCurrency from "../../Views/AddCurrency/AddCurrency";
import PrivateRoute from "../../components/PrivateRoute/PrivateRoute";
import NotFound from "../../Views/404/NotFound";

const Layout = (user) => {
  const [token, settoken] = useState(isAuthenticated());
  useEffect(() => {
    if (token) {
      const decodedToken = jwtDecode(token.token);
      console.log(decodedToken);
      // if (decodedToken.iat > Date.now()) {
      //   store.dispatch(logoutUser());
      //   window.location.href = "/login";
      // } else {
      store.dispatch({ type: SET_AUTHENTICATED });
      axios.defaults.headers.common["Authorization"] = `Bearer ${token.token}`;
      store.dispatch(getUserData());
      store.dispatch(getUserCart());

      console.log("AUTG", user);
      // }
    }
  }, []);
  const { role } = user.user.credentials;

  return (
    <main>
      <Router>
        <Navbar />

        <Switch>
          <Route path="/" exact component={Body}></Route>
          <Route path="/home" exact component={Body}></Route>
          <Route path="/product/:productId" exact component={ProductDetails} />
          <Route path="/search/:keyword" exact component={ProductSearch} />
          <AuthRoute path="/login" exact component={Login} />
          <AuthRoute path="/signup" exact component={Signup} />
          <CheckAuthRoute path="/cart" exact component={Cart} />
          <CheckAuthRoute path="/myreferals" exact component={MyReferal} />
          <PrivateRoute path="/order" exact component={Order} />
          <CheckAuthRoute path="/checkout" exact component={Checkout} />
          <CheckAuthRoute
            path="/thankyou/:orderId"
            exact
            component={ThankYou}
          />
          <CheckAuthRoute path="/addcurrency" exact component={AddCurrency} />
          <PrivateRoute path="/addproducts" exact component={AddProduct} />
          <PrivateRoute path="/editproduct/:id" exact component={EditProduct} />
          <PrivateRoute path="/viewproducts" exact component={ViewOwnProduct} />
          <Route path="/" exact component={NotFound} />
        </Switch>
        <Footer />
      </Router>
    </main>
  );
};

const mapStateToProps = (state) => ({
  user: state.user,
});

Layout.propTypes = {
  user: PropTypes.object,
};

export default connect(mapStateToProps)(Layout);
